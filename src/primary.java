import java.io.*;

public class primary {
	static int arrSize = 20;
	public static void main(String[] args) {
		int A[] = new int[arrSize];
		String div = " ";
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Insert number of elements: ");
		try {
			arrSize = Integer.parseInt(br.readLine());	
		} catch (IOException e) {
			e.printStackTrace();
		}
		for(int i=0; i<arrSize; i++){
			try {
				A[i] = Integer.parseInt(br.readLine());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Largest number: "+ max(A));
		System.out.println("Smallest number: "+ min(A));
		System.out.print("Palindromes:");
		for(int i=0; i<arrSize; i++){
			if(palindrome(A[i])){
				System.out.print(div + A[i]);
				div = ", ";
			}
		}
		System.out.println(""); div = " ";
		System.out.print("Prime numbers:");
		for(int i=0; i<arrSize; i++){
			if(prime(A[i])){
				System.out.print(div + A[i]);
				div = ", ";
			}
		}
	}
	
	static int max(int []arr){
		int result=arr[0];
		for(int i=0; i<arrSize; i++)
			result = Math.max(result, arr[i]);
		return result;
	}
	
	static int min(int []arr){
		int result=arr[0];
		for(int i=0; i<arrSize; i++)
			result = Math.min(result, arr[i]);
		return result;
	}
	
	static boolean palindrome(int num){
		Boolean result = false;
		String st1 = Integer.toString(num);
		String st2 = new StringBuilder(st1).reverse().toString();
		if(st1.equals(st2)) result = true;
		return result;
	}
	
	static boolean prime(int n) {
	    for(int i=2;2*i<n;i++) {
	        if(n%i==0)
	            return false;
	    }
	    return true;
	}
}
